import ClickOutside from "vue-click-outside";

class Calendar {
    constructor(selectedDate = {}, lang, dateFormat, disabledRange = {
        from: null,
        to: null,
    }) {
        this.currentDate = {
            year: selectedDate.year,
            month: selectedDate.month,
            date: selectedDate.date,
            firstDayOfWeek: selectedDate.firstDayOfWeek,
        };
        this.formatOptions = dateFormat;
        this.lang = lang;
        this.disabledRange = disabledRange;
        this.months = this.getMonths();
        this.days = this.getDays();
        this.firstDayOfMonth = this.getFirstDayOfMonth();
        this.dates = this.getDates();
        this.daysOfMonth = this.showDates();
    }

    getDays() {
        let days = [];
        let dayNumber;
        let name;
        for (let index = 1; index <= 7; index++) {
            name = new Date(this.currentDate.year, '0', index).toLocaleString(this.lang, {weekday: "short"});
            dayNumber = parseInt(new Date(this.currentDate.year, '0', index).getDay());
            if (this.currentDate.firstDayOfWeek === 'monday') {
                dayNumber = ((dayNumber - 1) + 7) % 7;
            }
            days.push({name, dayNumber});
        }
        this.sortDays(days);
        return days;
    }

    getMonths() {
        let months = [];
        for (let index = 0; index < 12; index++) {
            let name = new Date(this.currentDate.year, index).toLocaleString(this.lang, {month: "short"});
            months.push({index, name});
        }
        return months;
    }

    getDates() {
        let dates = [];
        let StatusDays = ((this.currentDate.year % 400 === 0) || ((this.currentDate.year % 4 === 0) && (this.currentDate.year % 100 !== 0))) ? 366 : 365;
        for (let index = 1; index <= StatusDays; index++) {
            let date = new Date(this.currentDate.year, '0', index);
            dates.push(date);
        }
        return dates;
    }

    showDates() {
        let countDateYears = this.dates.findIndex(item => new Date(item).getMonth() === this.currentDate.month);
        let daysOfMonth = this.dates.filter(item => new Date(item).getMonth() === this.currentDate.month);
        let dates = [];
        let prevDay = this.firstDayOfMonth;
        const prevYearLastOfMount = new Date(this.currentDate.year, '0', 1).getDate();
        if (this.currentDate.month === 0) {
            while (prevDay > 0) {
                let date = new Date(this.currentDate.year, '0', prevYearLastOfMount - prevDay);
                dates.push({
                    date: date,
                    isDayInMouth: false,
                });
                prevDay -= 1;
            }
        } else {
            while (prevDay > 0 && countDateYears - prevDay > 0) {
                let date = this.dates[countDateYears - prevDay];
                dates.push({
                    date,
                    isDayInMouth: false,
                });
                prevDay -= 1;
            }
        }
        daysOfMonth = daysOfMonth.map(item => ({
            date: item,
            isDayInMouth: true,
        }));
        dates = dates.concat(daysOfMonth);
        let nextDay = 0;
        let totalCount;
        while (dates.length % 7) {
            totalCount = countDateYears + daysOfMonth.length + nextDay;
            let StatusDays = ((this.currentDate.year % 400 === 0) || ((this.currentDate.year % 4 === 0) && (this.currentDate.year % 100 !== 0))) ? 366 : 365;
            let date = totalCount < StatusDays ? this.dates[countDateYears + daysOfMonth.length + nextDay] : new Date(this.currentDate.year + 1, '0', nextDay + 1);
            dates.push({
                date,
                isDayInMouth: false,
            });
            nextDay += 1;
        }
        dates = dates.map(item => ({
            mouth: new Date(item.date).getMonth(),
            day: new Date(item.date).getDate(),
            date: new Date(item.date).toLocaleDateString(this.lang, {...this.formatOptions}),
            fullDate: new Date(item.date),
            isDayInMouth: item.isDayInMouth,
            isUsable: (!this.disabledRange.from ? true : this.dateCompare(this.disabledRange.from, item.date, 'small')),
        }));
        return dates;
    }

    dateCompare(date1, date2) {
        return date1.setHours(0, 0, 0, 0).getTime() >= date2.setHours(0, 0, 0, 0).getTime();
    }

    getFirstDayOfMonth() {
        let firstDay = new Date(this.currentDate.year, this.currentDate.month, '01').getDay();
        if (this.currentDate.firstDayOfWeek.toLowerCase() === 'monday') {
            firstDay = ((firstDay - 1) + 7) % 7;
        }
        return parseInt(firstDay);
    }

    sortDays(days) {
        return days.sort(function (a, b) {return a.dayNumber - b.dayNumber;});
    }
}

export default {
    name: "Calendar",
    directives: { ClickOutside },
    data() {
        return {
            calendarWiew: 'day',
            isShowPicker: false,
            currentDate: {
                year: new Date().getFullYear(),
                month: new Date().getMonth(),
                date: new Date().getDate(),
                firstDayOfWeek: this.firstDayOfWeek,
            },
            selectedDate: null,
            value: new Date(),
        }
    },
    props: {
        /*
            Language for calendar. It Supports all languages.
            default: 'en'
        */
        lang: {
            type: String,
            default: 'en',
        },
        /*
            First day of week. Available value: monday or sunday.
            default: 'monday'.
        */
        firstDayOfWeek: {
            type: String,
            default: 'monday',
        },
        /*
            This is the format in which the selected date will be displayed to the user.
            day: "numeric" and "2-digit";
            month: "numeric", "2-digit", "narrow", "short" and "long";
            year: "numeric" and "2-digit";
            default: { day: '2-digit', month: 'long', year: 'numeric' }
        */
        dateFormat: {
            type: Object,
            default: function () {
                return {day: '2-digit', month: 'long', year: 'numeric'};
            },
        },
    },
    computed: {
        calendar() {
            return new Calendar(
                this.currentDate,
                this.lang,
                {...this.dateFormat},
            )
        },
        formattedValue() {
            return this.formatDate(this.selectedDate);
        }
    },
    methods: {
        clickOutside() {
            this.isShowPicker = false;
        },
        formatDate(value) {
            return new Date(value).toLocaleDateString(this.lang, {...this.dateFormat});
        },
        prevMount() {
            const currentDate = this.currentDate;
            currentDate.month = currentDate.month - 1;
            currentDate.year = (currentDate.month === -1)? currentDate.year - 1 : currentDate.year;
            currentDate.month = (currentDate.month === -1)? 11 : currentDate.month;
        },
        nextMount() {
            const currentDate = this.currentDate;
            currentDate.month = currentDate.month + 1;
            currentDate.year = (currentDate.month === 12)? currentDate.year + 1 : currentDate.year;
            currentDate.month = (currentDate.month === 12)? 0 : currentDate.month;
        },
        handlerDate(fullDate) {
            this.setDate(fullDate);
        },
        setDate(selectedDates) {
            this.$emit('input', selectedDates);
            this.selectedDate = selectedDates;
        }
    },
    mounted() {
        this.setDate(this.value);
    }
}