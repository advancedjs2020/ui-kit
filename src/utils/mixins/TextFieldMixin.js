export default {
  name: 'BaseTextField',
  model: {
    prop: 'value',
    event: 'change'
  },
  props: {
    /**
     * Type of input tag
     */
    type: {
      type: String,
      default: 'text'
    },
    /**
     * Class for inout container (prefix+input+postfix)
     * *prefix and postfix are optional
     */
    extendContainerClass: {
      type: String,
      default: ''
    },
    /**
     * Class for input element itself
     */
    extendInputClass: {
      type: String,
      default: ''
    },
    /**
     * Input value
     */
    value: [String, Number],

    /**
     * Input id
     */
    id: {
      type: String,
      default: ''
    },
    /**
     * Input name
     */
    name: {
      type: String,
      default: ''
    },
    /**
     * Input placeholder
     */
    placeholder: {
      type: String,
      default: ''
    },
    /**
     * Input status
     * Possible variants:  error, warning, success
     */
    status: {
      type: String,
      default: ''
    },
    /**
     * The ability to only read the input
     */
    readonly: {
      type: Boolean,
      default: false
    },
    /**
     * The ability to disable the input
     */
    disabled: {
      type: Boolean,
      default: false
    },
    /**
     * Autofocus on input
     */
    autofocus: {
      type: Boolean,
      default: false
    },
    /**
     * Required field before the sending
     */
    required: {
      type: Boolean,
      default: false
    },
    /**
     * The ability to expand input to full width
     */
    fitContainer: {
      type: Boolean,
      default: true
    },
    /**
     * Transition name before appearing
     */
    transitionName: {
      type: String,
      default: `fade`,
    },
    /**
     * Max number of symbols in the input field
     */
    maxlength: Number,
    /**
     * Min number of symbols in the input field
     */
    minlength: Number,

    /**
     * Max value that the user may enter in the inout field (for Number and Range types)
     */
    max: Number,
    /**
     * Min value that the user may enter in the inout field (for Number and Range types)
     */
    min: Number
  },
  data() {
    return {
      currentValue: this.value
    };
  },
  watch: {
    value(val) {
      this.setCurrentValue(val);
    }
  },
  methods: {
    handleFocus(evt) {
      /**
       * Emitted when the input is focused
       * @param event
       */
      this.$emit('focus', evt);
    },
    handleBlur(evt) {
      /**
       * Emitted when the input is unfocused
       * @param event
       */
      this.$emit('blur', evt);
    },
    handleInput(evt) {
      /**
       * Emitted when the input is typed
       * @param event
       */
      const value = evt.target.value;
      this.$emit('change', value);
      this.$emit('event-propagation', evt);
    },
    setCurrentValue(val) {
      /**
       * Setting current value to new one
       * @param value
       */
      if (val === this.currentValue) return;
      this.currentValue = val;
    }
  }
};
