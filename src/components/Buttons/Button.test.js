import Button from './Button.vue';
import { shallowMount } from "@vue/test-utils";

describe('Button', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(Button, {
      propsData: {
        size: 'lg',
      },
    })
  });

  describe('handleClick', () => {
    it('emits event on click', () => {
      wrapper.vm.handleClick();
      wrapper.vm.$nextTick().then(() => {
        expect(wrapper.emitted().click).toBeTruthy()
      }).catch(console.error)
    })
  });
});

