import Loader from './Loader.vue';
import { shallowMount } from "@vue/test-utils";

describe('Button', () => {
  describe('loaderStyle', () => {
    it('computes the height and width of loader', () => {
      const wrapper = shallowMount(Loader, {
        propsData: {
          size: 25
        }
      });
      expect(wrapper.vm.loaderStyle).toBe(`height:${wrapper.props().size}px;width:${wrapper.props().size}px`); //check computed property
      //expect(Loader.computed.loaderStyle.call(localThis)).toBe(`height:${localThis.size}px;width:${localThis.size}px`); //another way of checking computed property
      expect(wrapper.find(Loader).attributes().style).toBe(`height: ${wrapper.props().size}px; width: ${wrapper.props().size}px;`); //check styles of Loader
    })
  });
});
