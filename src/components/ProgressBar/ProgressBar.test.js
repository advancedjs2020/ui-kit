import ProgressBar from './ProgressBar.vue';
import { shallowMount } from "@vue/test-utils";
import Loader from '../Base/Loader';
describe('ProgressBar', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(ProgressBar, {
      propsData: {
        max: 50,
        value: 23,
        height: 5
      },
    });
  });
  describe('Computing Max value', () => {
    it('returns the max prop if is the number, 100 otherwise', () => {
      expect(wrapper.vm.computedMax).toBe(wrapper.props().max);
      expect(wrapper.find('.progress-bar__child').attributes()['aria-valuemax']).toBe(wrapper.props().max.toString());
    })
  });
});
