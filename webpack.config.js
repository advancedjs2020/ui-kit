const webpack = require("webpack");
const path = require("path");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");

const componentsPath = path.resolve(__dirname, './src/components');
const stylesPath = path.resolve(__dirname, './src/styles');
const assetsPath = path.resolve(__dirname, './src/assets');

const outputDirectory = "dist";

module.exports = {
  mode: "production",
  entry: {
    main: './index.js',
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, outputDirectory),
    library: '@mplt/ui-kit',
    libraryTarget: "umd",
    umdNamedDefine: true,
    globalObject: `(typeof self !== 'undefined' ? self : this)`,
  },
  plugins: [
    new CleanWebpackPlugin(),
    new VueLoaderPlugin(),
    new webpack.DefinePlugin({
      "typeof window": JSON.stringify("object")
    }),
    new MiniCssExtractPlugin({
      filename: 'style.css'
    })
  ],
  resolve: {
    modules: ["node_modules", "src"],
    alias: {
      '@mplt/ui-kit': componentsPath,
      'styles': stylesPath,
      'assets': assetsPath,
      vue: 'vue/dist/vue.esm.js'
    },
    extensions: [".webpack.js", ".web.js", ".js", ".css", ".vue"]
  },

  module: {
    rules: [
      {
        test: /\.vue?$/,
        loader: "vue-loader"
      },
      {
        test: /\.(woff|ttf|eot)$/,
        loader: "file-loader",
        options: {
          name: "assets/fonts/[folder]/[name].[ext]"
        }
      },
      {
        test: /\.(jpe?g|gif|png|svg)$/,
        loader: "file-loader",
        options: {
          name: "assets/images/[folder]/[name].[ext]"
        }
      },
      {
        test: /\.s?css$/,
        use: [
          'vue-style-loader',
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: '/\.js?$/',
        loader: "babel-loader"
      }
    ]
  },
  externals: [
    'vue',
    'vue-click-outside',
    'vue-slider-component'
  ]
};
