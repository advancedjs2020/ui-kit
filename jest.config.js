// jest.config.js
module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    "**/*.{js,vue}",
    "!**/node_modules/**",
    "!**/coverage/**",
    "!**/*.config.js",
    "!**/stories/**",
    "!**/public/**",
    "!**/storybook-static/**"
  ],

  snapshotSerializers: ["jest-serializer-vue"],
  moduleFileExtensions: ["js", "json", "vue"],
  transform: {
    "^.+\\.js$": "babel-jest",
    "^.+\\.vue$": "vue-jest"
  }
};
