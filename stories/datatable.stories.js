import Vue from "vue";
import DataTable from "../src/components/DataTable/DataTable.vue";
import Button from "../src/components/Buttons/Button";
import Dropdown from "../src/components/Dropdown/Dropdown";

export default { title: "DataTable" };

Vue.component("ui-button", DataTable);

// export const Default = () => "<ui-button>DataTable</ui-button>";

export const Styled = () => ({
  components: { DataTable },
  data() {
    return {
      columns: [
        {
          name: "goods",
          value: "Товар",
          sortable: false
        },
        {
          name: "monthAmount",
          value: "Количество месяцев",
          sortable: true,
          gridStyleWidth: "15vw"
        },
        {
          name: "transportAmount",
          value: "Количество ТС",
          sortable: true,
          gridStyleWidth: "10vw"
        },
        {
          name: "cost",
          value: "Сумма",
          sortable: true,
          gridStyleWidth: "10vw"
        },
        {
          name: "date",
          value: "Дата запуска",
          sortable: false,
          gridStyleWidth: "20vw"
        }
      ],
      rows: [
        {
          goods: Button,
          monthAmount: "blabla",
          chlen: "oIo",
          cost: 10500,
          date: "now"
        },
        { goods: Button, cost: 10050, date: "zavtra" },
        {
          goods: Button,
          monthAmount: Dropdown,
          transportAmount: Dropdown,
          cost: 100500,
          date: "popojje"
        }
      ]
    };
  },
  template: `
    <div>
      <DataTable :columns="columns" :rows="rows">
      </DataTable>
    </div>
  `
});
