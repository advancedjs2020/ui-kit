import Vue from 'vue';

import Header from '../src/components/Header/Header.vue';

export default {
  title: 'Header'
}

export const Default = () => ({
  components: { Header },
  template: `
  <div><Header /></div>
  `
});

export const LoggedIn = () => ({
  components: { Header },
  template: `
  <div><Header loggedIn /></div>
  `
});

export const BigBasket = () => ({
  components: { Header },
  template: `
    <div><Header loggedIn :basket="basket" /></div>
  `,
  data() {
    return {
      basket: { 
        big: true,
        quantity: 4,
        value: 5000
      }
    }
  }
});