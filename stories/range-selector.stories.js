import Vue from 'vue';
import RangeSelector from '../src/components/RangeSelector/RangeSelector';

export default {
  title: 'RangeSelector',
  component: RangeSelector,
  parameters: {
    componentSubtitle: 'The examples of RangeSelector component'
  }
};

Vue.component('RangeSelector', RangeSelector);

export const Default = () => ({
  template: `
    <div class="container mt-3 mb-3">
      <h2>RangeSelector component</h2>
      <RangeSelector 
          :value="[30, 60]" 
          :interval="1" 
          :min="0" 
          :max="100"
          :height="8"
          :showIntervalText="true"
      />
    </div>
  `
});
