import Vue from 'vue';

import BaseInput from '../src/components/InputComponent/BaseInput.vue';
import MaskedInput from '../src/components/InputComponent/MaskedInput.vue';

export default {
  title: 'Input',
  component: BaseInput,
  parameters: {
    componentSubtitle: 'The examples of input component'
  }
};

Vue.component('BaseInput', BaseInput);

export const Default = () => ({
  components: { BaseInput },
  template: `
    <div>
      <h2>Default input</h2>
      <BaseInput/>
    </div>
  `
});

export const Status = () => ({
  components: { BaseInput },
  data() {
    return {
      password: ''
    };
  },
  template: `
    <div class="container">
      <h2>Input with different status (border color)</h2>
      <div class="mb-4 row">
        <BaseInput status="error" id="error-input" type="password" placeholder="Password" v-model="password"/>
      </div>
      <div class="mb-4 row">
        <BaseInput status="warning" placeholder="Warning"/>
      </div>
      <div class="mb-4 row">
        <BaseInput status="success" placeholder="Ok" name="success"/>
      </div>
    </div>
  `
});
export const States = () => ({
  components: { BaseInput },
  template: `
    <div class="container">
      <h2>Input with different states</h2>
      <div class="mb-4 row">
        <BaseInput status="error" readonly placeholder="Readonly"/>
      </div>
      <div class="mb-4 row">
        <BaseInput disabled placeholder="Disabled" value="Value"/>
      </div>
      <div class="mb-4 row">
        <BaseInput autofocus placeholder="Autofocus"/>
      </div>
    </div>
  `
});
export const Prefix = () => ({
  components: { BaseInput },
  template: `
    <div class="container">
      <h2>Input with prefix slot</h2>
      <BaseInput placeholder="Александр Сергеевич Пушкин">
        <template slot="prefix"><span>Имя</span></template>
      </BaseInput>
    </div>
  `
});
export const Postfix = () => ({
  components: { BaseInput },
  template: `
    <div class="container">
      <h2>Input with postfix slot</h2>
      <BaseInput placeholder="Password">
        <template slot="postfix"><span>The password is too small</span></template>
      </BaseInput>
    </div>
  `
});

export const Mask = () => ({
  components: { MaskedInput },
  data() {
    return {
      phone: '',
      date: '',
      card: ''
    };
  },
  template: `
    <div>
      <h2>Masked input fields</h2>
      <div class="container d-flex flex-column justify-content-sm-around">
        <MaskedInput
            v-model="phone"
            mask="+7 (###) ### ##-##"
            placeholder="+7 (###) ### ##-##"
            extendContainerClass="mb-3"
        >
          <template slot="prefix"><span>Phone number</span></template>
        </MaskedInput>
        <MaskedInput
            v-model="date"
            mask="##.##.#### ##:##"
            placeholder="27.10.2016 23:15"
            extendContainerClass="mb-3"
        />
        <MaskedInput
            v-model="card"
            mask="#### #### #### ####"
            placeholder="4444 4444 4444 4444"
            extendContainerClass="mb-3"
        />
      </div>
    </div>
  `
});
