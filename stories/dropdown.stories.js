import Vue from 'vue';
import Dropdown from '../src/components/Dropdown/Dropdown.vue';

export default {
  title: 'Dropdown',
  component: Dropdown,
  parameters: {
    componentSubtitle: 'The examples of simple Dropdown component',
  },
};

Vue.component('Dropdown', Dropdown);

export const Default = () => ({
  data() {
    return{
      items: [
        {value: 1, text: 'First element'},
        {value: 2, text: 'Second element'},
        {value: 3, text: 'Third element'}
      ],
      active: {value: 1, text: 'First element'},
    }
  },
  template: `
    <div>
      <h2>Default button</h2>
      <Dropdown v-model="active" :items="items">
        <template slot="trigger">
          <p>{{active.text}}</p>
        </template>
      </Dropdown>
    </div>`
});

export const Styled = () => ({

  data() {
    return {
      items: [
        {value: 1, text: 'First element'},
        {value: 2, text: 'Second element'},
        {value: 3, text: 'Third element'}
      ],
      active: {value: 1, text: 'First element'},
      items2: [
        {value: 1, text: 'Jan'},
        {value: 2, text: 'Feb'},
        {value: 3, text: 'March'}
      ],
      active2: {value: 2, text: 'Feb'}
    };
  },
  template: `
    <div class="container">
      <div class="mb-4 row d-flex justify-content-sm-around">
        <div class="col col-xs-4">
          <Dropdown v-model="active" :items="items" :showChevron="true"/>
        </div>
        <div class="col col-xs-4">
          <Dropdown v-model="active" :items="items">
            <template slot="trigger">
              <p>{{active.text}}</p>
            </template>
          </Dropdown>
        </div>
        <div class="col col-xs-4">
          <Dropdown>
            <template slot="trigger">
              <p>Custom content</p>
            </template>
            <template slot="content">
              <div class="container">
                <div class="row">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium cupiditate distinctio, enim facilis inventore, nisi obcaecati officia possimus provident quas rem saepe sed sequi, soluta vel vero. Autem, est.</p>
                </div>
              </div>
            </template>
          </Dropdown>
        </div>
        <div class="col col-xs-4">
          <Dropdown v-model="active2" :items="items2" :showChevron="true">
            <template slot="trigger">
              <p>{{active2.text}}</p>
            </template>
          </Dropdown>
        </div>
      </div>
    </div>
  `
});
