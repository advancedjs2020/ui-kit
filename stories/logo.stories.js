import Vue from 'vue';

import Logo from '../src/components/Logo/Logo.vue';

export default {
  title: 'Logo',
  component: Logo,
  parameters: {
    docs: { disable: true }
  }
}
Vue.component('Logo', Logo);
export const Default = () => ({
  template: `<div><Logo /></div>`
});
