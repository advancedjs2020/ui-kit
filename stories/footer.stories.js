import Vue from 'vue';

import Footer from '../src/components/Footer/Footer.vue';

export default {
  title: 'Footer',
  component: Footer,
  parameters: {
    docs: { disable: true }
  }
};
Vue.component('Footer', Footer);
export const Default = () => ({
  template: `<div><Footer /></div>`
});
