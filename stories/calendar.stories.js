import Calendar from '../src/components/Calendar/Calendar.vue';

export default {title: 'Calendar'};

export const Styled = () => ({
    components: {Calendar},
    data: function () {
        return {
            example:
                {
                    lang: 'en',
                    dateFormat: { day: '2-digit', month: 'long', year: 'numeric'},
                    firstDayOfWeek: 'monday',
                }
        }
    },
    template: `
        <div>
            <Calendar
                    v-model="example.value"
                    :lang="example.lang"
                    :first-day-of-week="example.firstDayOfWeek"
                    :date-format="example.dateFormat"
            />
        </div>
    `
});
