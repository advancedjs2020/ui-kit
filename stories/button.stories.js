import Vue from 'vue';
import Button from '../src/components/Buttons/Button.vue';

export default {
  title: 'Button',
  component: Button,
  parameters: {
    componentSubtitle: 'The examples of simple button component',
  },
};

Vue.component('Button', Button);

export const Default = () => '<div><h2>Default button</h2><Button>Click</Button></div>';

export const Sizes = () => ({
  template: `
  <div>
    <h2>Buttons with different sizes</h2>
    <Button size="lg" success>Large</Button>
    <Button size="md" primary>Medium</Button>
    <Button size="sm" danger>Small</Button>
  </div>
  `
});

export const Styles = () => ({
  template: `
    <div>
      <div class="d-flex">
        <Button primary extend-class="mr-2">Primary</Button>
        <Button secondary extend-class="mr-2">Secondary</Button>
        <Button success badge="5" extend-class="mr-2">Success</Button>
        <Button warning extend-class="mr-2">Warning</Button>
        <Button danger extend-class="mr-2">Danger</Button>
        <Button info extend-class="mr-2">Info</Button>
      </div>
    </div>
  `
});

export const States = () => ({
  template: `
  <div>
    <h2>Buttons with different states</h2>
    <Button autofocus>Autofocus</Button>
    <Button disabled>Disabled</Button>
  </div>
  `
});

export const Outline = () => ({
  template: `
  <div>
    <h2>Buttons with outline</h2>
    <Button success outlined>Success</Button>
    <Button danger outlined>Danger</Button>
  </div>
  `
});

export const Loading = () => ({
  data() {
    return {
      loading: false
    };
  },
  template: `
  <div>
    <h2>Loading buttons</h2>
    <div class="mb-2">
      <label for="loading">
        <input id="loading" v-model="loading" type="checkbox" />
        Check loading
      </label>
    </div>
    <div>
      <Button warning outlined :loading="loading" size="lg">Button 1</Button>
      <Button :loading="loading" primary>Button 2</Button>
      <Button :loading="loading" badge="5" disabled>Button 3</Button>
      <Button :loading="loading" danger size="sm">Button 4</Button>
    </div>
  </div>
  `
});
