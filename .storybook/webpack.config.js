
const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const componentsPath = path.resolve(__dirname, '../src/components');
const stylesPath = path.resolve(__dirname, '../src/styles');
const assetsPath = path.resolve(__dirname, '../src/assets');

// Export a function. Accept the base config as the only param.
module.exports = async ({ config }) => {
  config.module.rules.push(
{
    test: /\.scss$/,
    use: [
      'style-loader',
      {
        loader: 'css-loader',
        options: {
          importLoaders: 2,
          modules: {
            mode: 'local',
            localIdentName: '[local]'
          }
        }
      },
      {
        loader: 'sass-loader',
        options: {
          sassOptions: {
            includePaths: [path.resolve(__dirname, '../src/')]
          }
        }
      }
    ],
    include: path.resolve(__dirname, '../src/')
  });

  config.resolve.alias = {
    '@mplt/ui-kit': componentsPath,
    'styles': stylesPath,
    'assets': assetsPath,
    vue: 'vue/dist/vue.esm.js'
  };

  return config;
};
