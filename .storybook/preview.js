import { addParameters } from '@storybook/vue';
import '../src/styles/bootstrap.utilities.css';

addParameters({
  docs: {
    inlineStories: true,
    iframeHeight: '120px',
  },
  options: {
    showPanel: true
  }
});
